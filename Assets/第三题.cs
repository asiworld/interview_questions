﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class 第三题 : MonoBehaviour
{
    public Transform m_tf;
    public Vector3 m_v3Pos;
    public Vector3 m_v3Rot;


    private void OnCollisionEnter(Collision collision)
    {
        m_tf.position = m_v3Pos;
        m_tf.eulerAngles = m_v3Rot;
    }
}
