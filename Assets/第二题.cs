﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class 第二题 : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Material m_material;
    public Color m_colorNormal;
    public Color m_highColor;
    public Color m_CurColor;

    public void OnPointerEnter(PointerEventData eventData)
    {
        m_CurColor = m_highColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        m_CurColor = m_colorNormal;
    }

    // Start is called before the first frame update
    void Start()
    {
        m_material = GetComponent<MeshRenderer>().material;
        m_colorNormal = m_material.GetColor("_Color");
        m_CurColor = m_colorNormal;
    }

    // Update is called once per frame
    void Update()
    {
        Color v_color = m_material.GetColor("_Color");
        m_material.SetColor("_Color", Color.Lerp(v_color, m_CurColor, Time.deltaTime * 5.0f));
    }
}
