﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class 第四题 : MonoBehaviour
{
    public Slider m_slider;
    public Text m_text;

    // Start is called before the first frame update
    void Start()
    {
        m_slider.onValueChanged.AddListener((float _arg0) => { m_text.text = _arg0.ToString(); });
    }
}
